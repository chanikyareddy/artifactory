

resource "aws_security_group" "my_sg" {
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8040"
        to_port     = "8040"
    }
    ingress {
    # chef default pport 
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    
  }

    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}
resource "aws_instance" "artifactory" {
    ami                         =  var.artifactory
    instance_type               = "t2.large"
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.artifactory.public_ip
    }

     provisioner "remote-exec" {
    inline = [
    "sudo apt-get update -y",
"sudo apt-get install openjdk-8-jdk -y",
"sudo wget -c https://bintray.com/jfrog/artifactory-pro-debs/download_file?file_path=pool%2Fmain%2Fj%2Fjfrog-artifactory-pro-deb%2Fjfrog-artifactory-pro-6.16.0.deb -O jfrog.deb",
"sudo dpkg -i jfrog.deb",
"sudo apt-get install net-tools",
"sudo systemctl start artifactory.service",
    ]
  }
   tags = {
    Name = "artifactory"
  }
}